using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MONEY : MonoBehaviour
{
    public Button[] buttons;
    public Text text1, texta, textb, textc, textd;
    public string Qoo;
    public string Ma, Mb, Mc, Md;


    public void Update()
    {
        if (Input.anyKeyDown)
        {
            KeyCode curkeyCode = KeyCode.None;
            for (int i = 0; i < System.Enum.GetValues(typeof(KeyCode)).Length; i++)
            {
                if (Input.GetKey((KeyCode)i))
                {
                    Debug.LogError((KeyCode)i);
                    curkeyCode = (KeyCode)i;
                    break;
                }
            }
            switch (curkeyCode)
            {
                case KeyCode.Alpha0:
                case KeyCode.Keypad0:
                    ShowButton(0);
                    break;
                case KeyCode.Alpha1:
                case KeyCode.Keypad1:
                    ShowButton(1);
                    break;
                case KeyCode.Alpha2:
                case KeyCode.Keypad2:
                    ShowButton(2);
                    break;
                case KeyCode.Alpha3:
                case KeyCode.Keypad3:
                    ShowButton(3);
                    break;
                case KeyCode.Alpha4:
                case KeyCode.Keypad4:
                    ShowButton(4);
                    break;
                case KeyCode.Alpha5:
                case KeyCode.Keypad5:
                    ShowButton(5);
                    break;
                case KeyCode.Alpha6:
                case KeyCode.Keypad6:
                    ShowButton(6);
                    break;
                case KeyCode.Alpha7:
                case KeyCode.Keypad7:
                    ShowButton(7);
                    break;
                case KeyCode.Alpha8:
                case KeyCode.Keypad8:
                    ShowButton(8);
                    break;
                case KeyCode.Alpha9:
                case KeyCode.Keypad9:
                    ShowButton(9);
                    break;
                case KeyCode.Delete:

                    del();
                    break;
                case KeyCode.KeypadEnter:
                case KeyCode.Return:
                    Equalbutt();
                    break;

            }

        }

    }
    public void ShowButton(int iButt)
    {
        Debug.Log(iButt);
        Qoo += iButt.ToString();
        text1.text = Qoo;
    }
    public void del()
    {
        Qoo = "";
        Ma = "";
        Mb = "";
        Mc = "";
        Md = "";
        text1.text = Qoo;
        texta.text = Ma;
        textb.text = Mb;
        textc.text = Mc;
        textd.text = Md;

    }
    public void Equalbutt()
    {
       
        int Money = int.Parse(Qoo);
        int a, b, c, d;
        a = Money / 1000 ;                     //千
        b = (Money - a * 1000) / 100;          //百
        c = (Money - a * 1000 - b * 100) / 10; //十
        d = Money % 10;                        //個

        string Ma = Convert.ToString(a);
        string Mb = Convert.ToString(b);
        string Mc = Convert.ToString(c);
        string Md = Convert.ToString(d);


        print("千位數是" + a);
        print("百位數是" + b);
        print("十位數是" + c);
        print("個位數是" + d);

        texta.text = Ma;
        textb.text = Mb;
        textc.text = Mc;
        textd.text = Md;
    }
}

