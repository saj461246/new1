using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayControl : MonoBehaviour
{
    private float Speed = 0.013f;   //移動速度
    private float Jump = 2.8f;      //跳躍
    private int Ground = 0;         //地板
    public int direction = 1;       //方向
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 position = transform.position;
        if (Input.GetKey(KeyCode.A))
        {
            Debug.Log(position);
            position.x -= Speed;
            direction = -1;
            transform.localScale = new Vector2(-1,1);

        }
        if (Input.GetKey(KeyCode.D))
        {
            position.x += Speed;
            direction = 1;
            transform.localScale = new Vector2(1, 1);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (Ground == 0)
            {
                Debug.Log("Jump");
                position.y += Jump;
            }
            
        }
        transform.position = position;
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Ground")
        {
            Debug.Log("Ground");
            Ground = 0;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Ground")
        {
            Debug.Log("Ground");
            Ground = 1;
        }
    }
    
}
