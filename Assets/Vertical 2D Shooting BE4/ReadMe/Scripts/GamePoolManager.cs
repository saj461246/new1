using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePoolManager : MonoBehaviour
{
    public static GamePoolManager Instance;
    private List<Bullet> bullets = new List<Bullet>();
    public void Awake()
    {
        Instance = this;
    }
    public void Do()
    {
        Debug.LogError(this.gameObject.name);
    }
    public Bullet Reuse()
    {
        if (bullets.Count > 0)
        {
            Bullet aBullet = null;
            for (int i = 0; i < bullets.Count; i++)
            {
                if (!bullets[i].gameObject.activeInHierarchy)
                {
                    aBullet = bullets[i];
                    break;
                }
            }
            if (aBullet == null)
            {
                aBullet = Instantiate(Resources.Load<Bullet>("Player Bullet 1"));
                bullets.Add(aBullet);
            }
            return aBullet;
        }
        else
        {
            var aBullet = Instantiate(Resources.Load<Bullet>("Player Bullet 1"));
            bullets.Add(aBullet);
            return aBullet;
        }
    }
}
