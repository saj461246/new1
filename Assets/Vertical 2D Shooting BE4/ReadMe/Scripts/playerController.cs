
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour
{
    
    public float HP = 10;
    public SpriteRenderer spriteRenderer;
    public Transform ShootPoint;
    public GameObject Bullet;
    public Animator anin;
    public float Speed = 0.5f;
    public Vector4 MoveArea;

    
    void Start()
    {
        MoveArea = new Vector4(2, 5, -2, -5);
        //for (int i = 0; i < 20; i++)
        //{
        //    var aBullet = Instantiate(Resources.Load<Bullet>("Player Bullet 1"));
        //    aBullet.gameObject.SetActive(false);

        //}
    }
    public void Update()
    {
        Vector3 Dir = Vector3.zero;
        if (Input.GetKeyDown(KeyCode.Q))
        {
            GamePoolManager.Instance.Do();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Bullet aBullet = GamePoolManager.Instance.Reuse();
            aBullet.gameObject.SetActive(true);
            aBullet.transform.position = ShootPoint.position;
            aBullet.Init(new Vector3(0, 1, 0), 3, 1, "Enemy", 6);
            //var aBullet = Instantiate(Resources.Load<Bullet>("Player Bullet 1"));
        }  
            if (Input.GetKey(KeyCode.W))
            {
               
                Dir.y = 1;
            }
            if (Input.GetKey(KeyCode.S))
            {
                Dir.y = -1;
            }
            if (Input.GetKey(KeyCode.A))
            {
                Dir.x = -1;
            }
            if (Input.GetKey(KeyCode.D))
            {
                Dir.x = 1;
            }
            transform.position += Dir * Time.deltaTime * Speed;
            anin.SetInteger("DirX", (int)Dir.x);
            //if (Dir.x > 0)
            //{
            //    anin.Play("Right");
            //}
            //else if (Dir.x < 0)
            //{
            //    anin.Play("Left");
            //}
            //else
            //{
            //    anin.Play("ldle");
            //}
            
            if (transform.position.x > MoveArea.x)
            {
                transform.position = new Vector3(MoveArea.x, transform.position.y);
            }
            if (transform.position.x < MoveArea.z)
            {
                transform.position = new Vector3(MoveArea.z, transform.position.y);
            }
            if (transform.position.y > MoveArea.y)
            {
                transform.position = new Vector3(transform.position.x, MoveArea.y);
            }
            if (transform.position.x < MoveArea.w)
            {
                transform.position = new Vector3(transform.position.x, MoveArea.w);
            }

        

    }
    public void OnHit(float iDamage)
    {
        HP -= iDamage;
        if (HP <= 0)
        {
            HP = 0;
            Destroy(gameObject);
        }
        else
        {
            StartCoroutine(OnHitMotion());
            //Anim.Play("Hit");
        }
    }
    public IEnumerator OnHitMotion()
    {
        spriteRenderer.color = Color.red;
        var pos = transform.position;
        transform.position += new Vector3(0, 0.1f, 0);
        yield return new WaitForSeconds(0.1f);
        spriteRenderer.color = Color.white;
        transform.position = pos;
        //Anim.Play("Hit");
        // Anim.Play("ldle");
    }
    
}

