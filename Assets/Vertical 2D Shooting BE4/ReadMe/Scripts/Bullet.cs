using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class Bullet : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Sprite[] sprites;
    private Vector3 Dir;
    private float Speed;
    private float Atk;
    protected string AttackTag;


    public void Init(Vector3 iDir, float iSpeed, float iAtk,string iAttackTag, int BulletType)
    {
        Dir = iDir;
        Speed = iSpeed;
        Atk = iAtk;
        AttackTag = iAttackTag;
        spriteRenderer.sprite = sprites[BulletType];
        spriteRenderer.flipY = iAttackTag == "Player";

    }
    void Update()
    {
        transform.position += Dir * Time.deltaTime * Speed;
        if (transform.position.y > 7 || transform.position.y < -7)
        {
            gameObject.SetActive(false);
            //Destroy(this.gameObject);
        }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Enemy")
        {
           var aEnemy = collision.GetComponent<EnemyBase>();
            aEnemy.OnHit(Atk);
            gameObject.SetActive(false);
            //Destroy(this.gameObject);
        }
        else if (collision.tag == "Player")
        {
            var aPlayerController = collision.GetComponent<playerController>();
            aPlayerController.OnHit(Atk);
            gameObject.SetActive(false);
        }
    }

    internal void Init(Vector3 vector3, int v1, int v2)
    {
        throw new NotImplementedException();
    }
}


