using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : MonoBehaviour
{
    public float HP = 5;
    public SpriteRenderer spriteRenderer;
    public Animator Anim;
    public Transform ShootPoint;
  

   protected void Start()
    {
        StartCoroutine(DoAttack());
    }

   protected  void Update()
    {
        
    }
   protected IEnumerator DoAttack()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            Bullet aBullet = GamePoolManager.Instance.Reuse();
            aBullet.gameObject.SetActive(true);
            aBullet.transform.position = ShootPoint.position;
            aBullet.Init(new Vector3(0, -1, 0), 3, 1, "Player",2);
        }
    }
    
    public void OnHit(float iDamage)
    {
        HP -= iDamage;
        if (HP <= 0)
        {
            HP = 0;
            Destroy(gameObject);
        }
        else 
        {
            StartCoroutine(OnHitMotion());
            //Anim.Play("Hit");
        }
    }
    public IEnumerator OnHitMotion()
    {
        spriteRenderer.color = Color.red;
        var pos = transform.position;
        transform.position += new Vector3(0, 0.1f, 0);
        yield return new WaitForSeconds(0.1f);
        spriteRenderer.color = Color.white;
        transform.position = pos;
        //Anim.Play("Hit");
        // Anim.Play("ldle");
    }
}
